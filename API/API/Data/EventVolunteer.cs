namespace API.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EventVolunteer")]
    public partial class EventVolunteer
    {
        public int ID { get; set; }

        public int? AccountID { get; set; }

        public int? EventID { get; set; }

        public DateTime? TimeCreated { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public virtual Account Account { get; set; }

        public virtual Event Event { get; set; }
    }
}
