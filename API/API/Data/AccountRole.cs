namespace API.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccountRole")]
    public partial class AccountRole
    {
        public int ID { get; set; }

        public int? AccountID { get; set; }

        public int? RoleID { get; set; }

        public virtual Account Account { get; set; }

        public virtual Role Role { get; set; }
    }
}
