namespace API.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrganizationMemberRole")]
    public partial class OrganizationMemberRole
    {
        public int ID { get; set; }

        public int? OrganizationRoleID { get; set; }

        public int? OrganizationMemberID { get; set; }

        public bool? Status { get; set; }

        public virtual OrganizationMember OrganizationMember { get; set; }

        public virtual OrganizationRole OrganizationRole { get; set; }
    }
}
